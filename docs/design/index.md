Design {#design}
======

This section outlines the Autoware.Auto design principles, and links to relevant design documents
for each Autoware.Auto feature.

- @subpage autoware-drivers-design
- @subpage autoware-fusion-design
